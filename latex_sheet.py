from pylatex import Document, Section, Subsection, Tabular, Math, TikZ, Axis, \
    Plot, Figure, Matrix, Alignat, MiniPage, LineBreak, VerticalSpace, Command
from pylatex.utils import italic
import numpy as np

import csv

def generate_monster_rows(level):
    rows = []
    min_level = level-1
    if(min_level>1):
        rows.append([f"<={min_level-1}", f"D(L)", "", ""])
    offset = 0
    if level % 2 == 0:
        offset = 1
    for i in range(1,6):
        #print(i)
        L = i+min_level-1
        row = [L]
        N = max(0, i-offset) % 3 + offset
        E  = int((i - offset) / 3)
        #N = max(0, N-E)
        total_level = int(level*(N+E))
        D = max(0, (i-(N+E)))
        if E>0:
            row.append(f'E{E}')
        if N>0:
            row.append(f'N{N}')
        if D>0:
            row.append(f'D{L-3}')
        for j in range(4-len(row)):
            row.append('')
        rows.append(row)

    rows[-1][0]="+"+str(rows[-1][0])
    rows[-1][3]="D(L-5)"
    return rows

def generate_boss_rows(level):
    rows = []
    min_level = level-1
    if(min_level>1):
        rows.append([f"<={min_level-1}", f"D(L)", f"D(L-3)", ""])
    offset = 0
    if level % 2 == 0:
        offset = 1
    for i in range(1,4):
        #print(i)
        L = i+min_level-1
        row = [L, "B1"]
        #N = max(0, N-E)s
        if i>1:
            row.append(f'D{L-3}')
        if i>=3:
            row.append(f'D{L-4}')
        for j in range(4-len(row)):
            row.append('')
        rows.append(row)

    rows[-1][0]="+"+str(rows[-1][0])
    rows[-1][3]="D(L-5)"
    return rows

def generate_labels():
    geometry_options = {"margin": "0.0in"}
    doc = Document(geometry_options=geometry_options)

    doc.change_document_style("empty")

    doc.documentclass = Command('documentclass',options=['10pt','landscape'],arguments='article')

    with open('input/autogen/monsters.csv', 'r', encoding='utf-8-sig') as csvfile:
        #spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        reader = csv.DictReader(csvfile)

        for i, row in enumerate(reader):
            #print(row)
            with doc.create(MiniPage(width=r"0.166\textwidth")):
                name = row['Name']
                lvl = row['Level']
                title = f"{name} - Level {lvl}"
                #print(title)
                doc.append(title)
                doc.append("\n")
                with doc.create(Tabular('r|ccl', col_space="4pt")) as table:
                    if "BOSS" in row['Statline']:
                        rows = generate_boss_rows(int(lvl))
                    else:
                        rows = generate_monster_rows(int(lvl))
                    table.add_hline()
                    for r in rows:
                        table.add_row(r)
            if (i % 6) == 0:
                doc.append(VerticalSpace("20pt"))
                doc.append(LineBreak())

    doc.generate_pdf("minipage", clean_tex=False)


generate_labels()
