from optparse import OptionParser

from dicebuilder.cardgenerator import CardGenerator,Card,SheetGenerator
from os import listdir
import os
import math
### Card Configuration
CONFIG = {
  "card_dimensions_px":
      {'monster':(1050, 875),
       'system':(1050, 675)},
  "card_padding_px": 32,
}

def munge_text(text):
  return text.replace("_", "__________")

def main():
  parser = OptionParser()
  parser.add_option("-c", "--color", type="string", default="white",
                    help="color scheme. white or black")
  parser.add_option("-f", "--footer_text", type="string", default="DustMech v0.4.05",
                    help="text for footer.")
  parser.add_option("-t", "--header_text", type="string", default="Crazy Attack Name",
                    help="text for header.")
  parser.add_option("-p", "--power_cost", type="string", default="3",
                    help="power cost.")
  parser.add_option("-k", "--keywords", type="string", default="mech,melee",
                    help="power keywords")
  parser.add_option("-m", "--move_cost", type="string", default="2",
                    help="move cost.")
  parser.add_option("-i", "--input_file", type="string", default="input/demo_cards",
                    help="file to read from")
  parser.add_option("-l", "--footer_logo", type="string", default="source/logo.png",
                    help="logo for footer.")
  parser.add_option("-s", "--save_folder", type="string", default="cards",
                    help="folder to save output to")

  options, args = parser.parse_args()

  config = CONFIG
  config["color"] = options.color
  config["save_folder"] = options.save_folder
#  config["header_text"] = options.header_text
#  config["power_cost"]= options.power_cost
#  config["move_cost"]= options.move_cost
#  config["keywords"]= options.keywords
#  config["footer_text"] = options.footer_text
#  config["footer_logo"] = options.footer_logo
  config["double_sided"] = True

  onlyfiles = [f for f in listdir('input/autogen') if f.endswith(".csv")]
  tabletop_sim_sheet = True
  if onlyfiles is None:
      print("No files to generate:")

  for filename in onlyfiles:

      print("Generating from...."+filename)

      cg = CardGenerator(config)
      sheet = SheetGenerator(config)
      sheet.sheet_name = os.path.splitext(filename)[0]

      if tabletop_sim_sheet:
        sheet.double_sided = False
        sheet.num_columns=10
        sheet.num_rows=4
        sheet.right_x_pad=0
        sheet.left_x_pad=0
        sheet.bottom_y_pad=0
        sheet.top_y_pad=0
        sheet.file_type='PNG'

      import csv


      with open('input/autogen/'+filename, 'r', encoding='utf-8-sig') as csvfile:
         #spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
         reader = csv.DictReader(csvfile)

         for row in reader:
             card = Card(row)
             card.front_img = cg.writeCard(card)
             card.back_img = cg.makeBack()
             sheet.add_card(card)

      if len(sheet.cards) > 30:
          sheet.num_columns = 9
      else:
          sheet.num_columns = 5
      sheet.num_rows=math.ceil(len(sheet.cards)/sheet.num_columns)
      type = 'system'
      print(card.type)
      if card.type == 'Monster':
          type = 'monster'
      print(f'type = {type}')
      sheet.writeSheets(type)

  #with open(options.input_file, 'r') as f:
  #  for card in f.readlines():
  #    text = munge_text(card)
  #    cg.writeCard(text)

  #cg.writeBack()

  print("Created {} cards from {}, saved to {}".format(options.color, options.input_file, options.save_folder))

if __name__ == '__main__':
    main()
