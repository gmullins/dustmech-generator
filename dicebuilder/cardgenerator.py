import os
import PIL
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
from PIL import ImageColor
import textwrap
import re


CHARS_PER_LINE = 41
FONT_PATH = "/Library/Fonts/Arial.ttf"
BODY_SIZE = 50
FOOTER_SIZE = 18
HEADER_FONT_SIZE = 60
HEADER_SIZE = 120
KEYWORD_SIZE = 32
DICE_TEXT_SIZE = 55
RESULT_TEXT_SIZE = 60
STATLINE_TEXT_SIZE = 40
DICE_SIZE = 150

#Name,Type,Keywords,Power,Move,Body,Discard
ICONS = {'Damage': 'input/swords.png',
         'Move': 'input/boot.png',
         'Range': 'input/bow2.png',
         'Target':'input/target.png',
         'Burn':'input/radioactive.png',
         }

class Card(object):
  def __init__(self,config):
    self.name = config['Name']
    self.type = config['Type']
    self.keywords_text = self.get_default(config,"Keywords","")
    self.load = self.get_default(config,"Load","0")
    self.health = self.get_default(config,"Health","0")
    self.cooldown = self.get_default(config,"Cooldown","0")
    self.diceline = self.get_default(config,"Dice","")
    self.resultline = self.get_default(config,"Result","")
    self.body_text = self.get_default(config,"Body","Text goes here")
    self.initiative = self.get_default(config,"Initiative","1")
    self.statline = self.get_default(config,"Statline","")
    self.threatline = self.get_default(config,"Threatline","")
    self.targetline = self.get_default(config,"Targetline","")

    ability1 = self.get_default(config,"Ability1","")
    ability2 = self.get_default(config,"Ability2","")
    ability3 = self.get_default(config,"Ability3","")
    ability4 = self.get_default(config,"Ability4","")
    ability5 = self.get_default(config,"Ability5","")
    ability6 = self.get_default(config,"Ability6","")

    self.ability_list = [ability1, ability2, ability3, ability4, ability5, ability6]

    self.front_img = None
    self.back_img = None

  def get_default(self,config,field,default):
    new_line = config.get(field,default)
    if(new_line == '' or new_line == '-' ):
          new_line = None
    return new_line


class SheetGenerator(object):
  def __init__(self,config):
    self.num_rows = 2
    self.num_columns = 4
    self.file_type = "PDF"
    self.card_dimensions = config["card_dimensions_px"]
    self.inter_x_pad = config.get('inter_x_pad',0)
    self.inter_y_pad = config.get('inter_y_pad',0)
    self.left_x_pad = config.get('inter_x_buff',130)
    self.right_x_pad = config.get('inter_x_buff',265)
    self.bottom_y_pad = config.get('inter_x_buff',150)
    self.top_y_pad = config.get('inter_x_buff',140)
    self.double_sided = config.get('double_sided',False)

    self.type = 'system'
    self.cards = []
    self.index = 0
    self.color = config['color']
    self.save_folder = config["save_folder"]
    self.sheet_list = []
    self.sheet_name = "CardSheet"

  @property
  def background_color(self):
    if self.color == "white":
      return (255, 255, 255, 255)
    else:
      return (0, 0, 0, 255)

  def add_card(self,card):
    self.cards.append(card)

  def makeSheets(self):
    print(f'Making sheet of type {self.type}')
    X_SIZE = self.card_dimensions[self.type][0]*self.num_columns+self.left_x_pad+self.right_x_pad+3*self.inter_x_pad
    Y_SIZE = self.card_dimensions[self.type][1]*self.num_rows+self.top_y_pad+self.bottom_y_pad+self.inter_y_pad
    sheet = Image.new("RGB", (X_SIZE, Y_SIZE), self.background_color)
    draw = ImageDraw.Draw(sheet)

    if(self.double_sided):
      backsheet = Image.new("RGB", (X_SIZE, Y_SIZE), self.background_color)
      draw_back = ImageDraw.Draw(backsheet)

    x_offset = self.left_x_pad
    y_offset = self.top_y_pad
    for card in self.cards:
      card_img = card.front_img
      sheet.paste(card_img, (x_offset, y_offset), card_img)
      draw.line((x_offset,0,x_offset,Y_SIZE),fill="rgb(10,18,25)",width=5)
      draw.line((x_offset+card_img.size[0],0,x_offset+card_img.size[0],Y_SIZE),fill="rgb(10,18,25)",width=5)
      draw.line((0,y_offset,X_SIZE,y_offset),fill="rgb(10,18,25)",width=5)
      draw.line((0,y_offset+card_img.size[1],X_SIZE,y_offset+card_img.size[1]),fill="rgb(10,18,25)",width=5)
      if self.double_sided:
        card_img = card.back_img
        backsheet.paste(card_img, (x_offset, y_offset), card_img)
        draw_back.line((x_offset,0,x_offset,Y_SIZE),fill="rgb(10,18,25)",width=5)
        draw_back.line((x_offset+card_img.size[0],0,x_offset+card_img.size[0],Y_SIZE),fill="rgb(10,18,25)",width=5)
        draw_back.line((0,y_offset,X_SIZE,y_offset),fill="rgb(10,18,25)",width=5)
        draw_back.line((0,y_offset+card_img.size[1],X_SIZE,y_offset+card_img.size[1]),fill="rgb(10,18,25)",width=5)

      x_offset +=card_img.size[0]+self.inter_x_pad
      if(x_offset+self.right_x_pad >= X_SIZE):
        x_offset=self.left_x_pad
        y_offset +=card_img.size[1]+self.inter_y_pad
      if(y_offset+self.bottom_y_pad >= Y_SIZE):
        self.sheet_list.append(sheet)
        if self.double_sided:
          self.sheet_list.append(backsheet)
        y_offset=self.top_y_pad
        x_offset=self.left_x_pad
        sheet = Image.new("RGB", (X_SIZE, Y_SIZE), self.background_color)
        draw = ImageDraw.Draw(sheet)
        if(self.double_sided):
          backsheet = Image.new("RGB", (X_SIZE, Y_SIZE), self.background_color)
          draw_back = ImageDraw.Draw(backsheet)


    self.sheet_list.append(sheet)
    if self.double_sided:
      self.sheet_list.append(backsheet)


  def writeSheets(self,type='system'):
    filename = u"{}.pdf".format(self.sheet_name)
    self.index += 1
    self.type = type
    self.makeSheets()

    if self.file_type == 'PDF':
      self.sheet_list[0].save(os.path.join(self.save_folder, filename),'PDF', dpi=(300, 300),save_all=True, append_images=self.sheet_list[1:])

    if self.file_type == 'PNG':
      for i,sheet in enumerate(self.sheet_list):
        filename = u"{}.{}.png".format(self.sheet_name,i)
        sheet.save(os.path.join(self.save_folder, filename))

class CardGenerator(object):
  def __init__(self, config):
    self.save_folder = config["save_folder"]
    self.color = config["color"]
    self.card_dimensions = config["card_dimensions_px"]

    self.card_colors = {'Offense':"rgb(155,155,255)",'Defense':"rgb(235,235,0)",
                        "Utility":"rgb(235,40,100)",'Mobility':"rgb(50,235,130)",
                        "Basic":"rgb(150,150,150)","Generator":"rgb(235,30,234)"}
    self.icons = ICONS
    self.card_body_size = {'default':BODY_SIZE, 'System':BODY_SIZE,'Energy':270}

    self.footer_text = "DustMech v0.3.13"
    self.index = 0
    self.padding = config["card_padding_px"]

    super(CardGenerator, self).__init__()

  def make_statblock(self,load,health,cooldown):
    pass

  def make_double_move_icon(self):
    return self.make_move_icon()

  @property
  def background_color(self):
    if self.color == "white":
      return (255, 255, 255, 255)
    else:
      return (0, 0, 0, 255)

  @property
  def foreground_color(self):
    if self.color == "white":
      return (0, 0, 0, 255)
    else:
      return (255, 255, 255, 255)

  def makeHeader(self, card_data, card_img, draw, yoffset = 0):
    header_font = ImageFont.truetype("/Library/Fonts/Arial.ttf", HEADER_FONT_SIZE)
    stats_font = ImageFont.truetype('/Users/mullige1/Library/Fonts/Symbola.ttf', encoding="Apple Roman", size=RESULT_TEXT_SIZE)

    offset = yoffset+self.padding
    header_bottom = offset + header_font.getsize(card_data.name)[1]
    draw.rectangle((0, 0, card_img.size[0], HEADER_SIZE), fill=(80, 80, 80, 255))
    draw.rectangle((0, 0, card_img.size[0], 20), fill=self.card_colors[card_data.type])


    autoSizeText(draw, card_data.name, (self.padding, self.padding), (card_img.size[0]-self.padding,HEADER_SIZE),
                 default_size=HEADER_FONT_SIZE, color = (255,255,255,255))


    offset = offset + HEADER_SIZE
    box_size = HEADER_SIZE-2
    # Load box
    x0 = card_img.size[0]-3*box_size
    x1 = card_img.size[0]-2*box_size
    draw.rectangle((x0, yoffset, x1, HEADER_SIZE),fill="rgb(52, 235, 226)", outline="rgb(0,0,0)", width=4)
    load_text = '\u2693'+card_data.load
    w, _ = draw.textsize(load_text, font=stats_font)
    draw.text((x0+int(box_size/2-w/2), yoffset+25), load_text, font=stats_font, fill=(0,0,0,255))
    # Health Box
    x0 = card_img.size[0]-2*box_size
    x1 = card_img.size[0]-1*box_size
    draw.rectangle((x0, yoffset, x1, HEADER_SIZE),fill="rgb(92, 235, 52)", outline="rgb(0,0,0)", width=4)
    health_text = '\u2661' + card_data.health
    w, _ = draw.textsize(health_text, font=stats_font)
    draw.text((x0+int(box_size/2-w/2), yoffset+25), health_text, font=stats_font, fill=(0,0,0,255))

    # Cooldown Box
    x0 = card_img.size[0]-1*box_size
    x1 = card_img.size[0]-0*box_size
    cool_text = '\u21BA' + card_data.cooldown
    w, _ = draw.textsize(cool_text, font=stats_font)
    draw.rectangle((x0, yoffset, x1, HEADER_SIZE),fill="rgb(235, 155, 52)", outline="rgb(0,0,0)", width=4)
    draw.text((x0+int(box_size/2-w/2), yoffset+25), cool_text, font=stats_font, fill=(0,0,0,255))


    draw.line((0, offset, card_img.size[0], offset), fill=128)
    return offset

  def makeMonsterHeader(self, card_data, card_img, draw, yoffset = 0):
    header_font = ImageFont.truetype("/Library/Fonts/Arial.ttf", HEADER_FONT_SIZE)
    stats_font = ImageFont.truetype('/Users/mullige1/Library/Fonts/Symbola.ttf', encoding="Apple Roman", size=STATLINE_TEXT_SIZE)

    MONSTER_HEADER_SIZE = int(0.75*HEADER_SIZE)
    header_y = 0
    stat_y = header_y + MONSTER_HEADER_SIZE
    target_y = stat_y + MONSTER_HEADER_SIZE


    initiative_text = 'Init:'+card_data.initiative
    w, _ = draw.textsize(initiative_text, font=header_font)

    offset = yoffset+self.padding
    header_bottom = offset + header_font.getsize(card_data.name)[1]
    #HEADER BLOCK
    draw.rectangle((0, header_y, card_img.size[0], stat_y), fill=(185,20,20,255))
    #STAT BLOCK
    draw.rectangle((0, stat_y, card_img.size[0], target_y), fill=(125,125,125,255),outline=(0,0,0,255))
    #TARGET BLOCK
    draw.rectangle((0, target_y, card_img.size[0], target_y+MONSTER_HEADER_SIZE), fill=(185,185,185,255), outline=(0,0,0,255))
    #THREAT BLOCK
    draw.rectangle((0, card_img.size[1]-HEADER_SIZE, card_img.size[0], card_img.size[1]), fill=(239, 255, 10,255), outline=(0,0,0,255))

    #NAME
    autoSizeTextMultiline(draw, card_data.name, (self.padding, header_y+10),
                          (card_img.size[0]-self.padding-w,MONSTER_HEADER_SIZE), default_size=HEADER_FONT_SIZE,
                          color=(255,255,255,255))
    #Initiative
    autoSizeTextMultiline(draw, initiative_text, (card_img.size[0]-self.padding-w, header_y+10),
                          (w+self.padding,HEADER_SIZE), default_size=HEADER_FONT_SIZE,
                          color=(255,255,255,255))
    #STATS
    autoSizeTextMultiline(draw, replace_unicode_icons(card_data.statline), (0, stat_y), (card_img.size[0], MONSTER_HEADER_SIZE),
                          fonttype ='/Users/mullige1/Library/Fonts/Symbola.ttf', default_size = HEADER_FONT_SIZE,
                          x_center = True, y_center = True,
                          color=(0,0,0,255))
    #TARGET
    autoSizeTextMultiline(draw, replace_unicode_icons(card_data.targetline), (0, target_y), (card_img.size[0], MONSTER_HEADER_SIZE),
                          fonttype ='/Users/mullige1/Library/Fonts/Symbola.ttf', default_size = HEADER_FONT_SIZE,
                          x_center = True, y_center = True)
    #THREAT
    autoSizeTextMultiline(draw, replace_unicode_icons(card_data.threatline), (0, card_img.size[1]-100), (card_img.size[0], 100),
                          fonttype ='/Users/mullige1/Library/Fonts/Symbola.ttf', default_size = HEADER_FONT_SIZE,
                          x_center = True, y_center = True)

  def makeKeywords(self, card_data, card, draw, yoffset = 628):
    keyword_font = ImageFont.truetype("/Library/Fonts/Arial.ttf", KEYWORD_SIZE)
    card_type_font = ImageFont.truetype("/Library/Fonts/Arial.ttf", int(KEYWORD_SIZE*1.25))

    card_type_bottom = card_type_font.getsize(card_data.type)[1]
    draw.rectangle((0, yoffset, card.size[0], yoffset+card_type_bottom+5),fill="rgb(200,200,200)")
    draw.text((self.padding, yoffset), card_data.type, font=card_type_font, fill=self.foreground_color)
    if card_data.keywords_text is not None:
      keyword_width = keyword_font.getsize(card_data.keywords_text)[0]
      keyword_bottom = keyword_font.getsize(card_data.keywords_text)[1]
      draw.text((card.size[0]-keyword_width-self.padding/3, yoffset+card_type_bottom-keyword_bottom), card_data.keywords_text, font=keyword_font, fill=self.foreground_color)

    yoffset = yoffset+card_type_bottom
    return yoffset

  def makeDiceLine(self, card_data):#, card_img, draw, yoffset = 0):

    dice_list = parse_dice_line(card_data)

    icon = make_dice_line_icon(dice_list)

    return icon


  def makeBottomText(self, card_data, card_img, draw):
    bottom_font = ImageFont.truetype("/Library/Fonts/Arial.ttf", HEADER_FONT_SIZE)

    bottom_text_width = bottom_font.getsize(card_data.bottom_text)[0]
    bottom_text_height = bottom_font.getsize(card_data.bottom_text)[1]

    draw.rectangle((0, card_img.size[1] - bottom_text_height - self.padding / 2, card_img.size[0], card_img.size[1]), fill="rgb(200,100,100)")
    offset = (self.padding / 3, card_img.size[1] - bottom_text_height - self.padding / 3)
    self.printWithIcons(card_img,draw,offset, card_data.bottom_text,bottom_font)

    #draw.text((self.padding / 3, card_img.size[1] - bottom_text_height - self.padding / 3), card_data.bottom_text, font=bottom_font, fill=self.foreground_color)


  def makeCard(self,card_data):



    if card_data.type in self.card_colors.keys():
      return self.makeSystemCard(card_data)

    elif card_data.type == 'Monster':
     return self.makeMonsterCard(card_data)

    else:
      print(f'Card type not recognized {card_data.type}')
      return None

  def makeMonsterCard(self,card_data):
    card_img = Image.new("RGBA", (self.card_dimensions['monster'][0], self.card_dimensions['monster'][1]), self.background_color)
    draw = ImageDraw.Draw(card_img)
    self.makeMonsterHeader(card_data, card_img, draw)
    #self.makeMonsterStatline(card_data, card_img, draw)
    #self.makeMonsterThreatline(card_data, card_img, draw)

    for i,a in enumerate(card_data.ability_list):
      if a is None or len(a) <= 1:
        continue
      x_offset = 0
      if i==0 or i==3:
        y_offset = int(2.25*HEADER_SIZE)

      if i>2:
        x_offset += int(card_img.size[0]/2)
      height = 170
      if i<5 and (card_data.ability_list[i+1] == None or len(card_data.ability_list[i+1]) <= 1):
        print('skip 1')
        height += 170
        if i<4 and (card_data.ability_list[i+2] == None or len(card_data.ability_list[i+2]) <= 1):
          print('skip 2')
          height += 170
      print(a, len(a))
      ability_icon = make_monster_ability_icon(a, int(card_img.size[0]/2), height)
      card_img.paste(ability_icon, (x_offset, y_offset), ability_icon)

      y_offset += height



    return card_img


  def makeSystemCard(self,card_data):
    card_img = Image.new("RGBA", (self.card_dimensions['system'][0], self.card_dimensions['system'][1]), self.background_color)
    draw = ImageDraw.Draw(card_img)

    offset = self.makeHeader(card_data,card_img,draw)
    content_height = 0
    dice_height = 0
    body_height = 0
    dice_body_gap = 0
    if card_data.diceline:
      dice_icon = self.makeDiceLine(card_data)
      dice_height = dice_icon.size[1]

    if card_data.body_text:
      body_height = 400 - dice_height
      print(f'body_height={body_height}')
      body_icon=make_body_icon(card_data.body_text, self.padding, (self.card_dimensions['system'][0], body_height) )
      body_height = body_icon.size[1]

    content_height = dice_height + body_height + dice_body_gap
    dice_offset_y = int(offset + 250 - content_height/2)

    if card_data.diceline:
      dice_offset_x = int(card_img.size[0] / 2 - dice_icon.size[0]/2)
      card_img.paste(dice_icon, (dice_offset_x, dice_offset_y),dice_icon)

    if card_data.body_text:
      body_dice_offset_x = 0
      body_offset_y = dice_offset_y + dice_height + dice_body_gap
      card_img.paste(body_icon, (body_dice_offset_x, body_offset_y),body_icon)

    offset = self.makeKeywords(card_data,card_img,draw)

    offset += self.padding
    # draw words

    return card_img

  def writeCard(self, card_data):
    filename = u"{}.{}.0.png".format(card_data.type,self.index)
    self.index += 1
    card = self.makeCard(card_data)
    card.save(os.path.join(self.save_folder, filename))
    if self.index==0:
      card_back = self.makeBack()
      filename = u"{}.1.png".format(self.index)
      card_back.save(os.path.join(self.save_folder, filename))

    return card

  def makeBack(self):
    card_img = Image.new("RGBA", (self.card_dimensions['system'][0], self.card_dimensions['system'][1]), self.background_color)
    draw = ImageDraw.Draw(card_img)

    # draw words
    body_font = ImageFont.truetype(FONT_PATH, BODY_SIZE * 2)

    placeholder_logo = Image.open('input/wrench_cog_icon.png','r')

    offset_x = int(card_img.size[0]/2-placeholder_logo.size[0]/2)
    offset_y = int(card_img.size[1]/2-placeholder_logo.size[1]/2)
    card_img.paste(placeholder_logo, (offset_x, offset_y), placeholder_logo)

    return card_img

  def writeBack(self):
    card = self.makeBack()
    card.save(os.path.join(self.save_folder, "back.{}.png".format(self.color)))


def parse_dice_line(card_data):
  dice_line = card_data.diceline
  result_line = card_data.resultline
  if not dice_line:
    return []
  #print(result_line)
  dice_line_list = re.findall("(\[.*?\])|(\{.*?\})|(\_.*?\_)",dice_line)
  result_list = []
  if result_line:
    result_list = re.findall("(\[.*?\])",result_line)
  #assert len(dice_line_list) != len(result_list), f'Dice Line {dice_line} does not match Result line {result_line}'

  n_dice = len(dice_line_list)

  dice_list = []
  k = 0
  for i in range(n_dice):
    D = {}
    for j in range(3):
      if dice_line_list[i][j] is not '':
        break
    D['height'] = DICE_SIZE
    D['result'] = ''

    if j == 0:
      #Regular die
      D['type'] = 'die'
      D['width'] = DICE_SIZE
      if len(result_list) > k:
        D['result'] = result_list[k]
        D['height'] += 100
      k+=1
    elif j == 1:
      #Optional die
      D['type'] = 'optional'
      D['width'] = DICE_SIZE
      if len(result_list) > k:
        D['result'] = result_list[k]
        D['height'] += 100
      k+=1
    elif j == 2:
      #Interstitial Text
      D['type'] = 'text'
      D['width'] = 20

    D['text'] = dice_line_list[i][j][1:-1]
    print(result_list)

    dice_list.append(D)


  return dice_list


def make_body_icon(body_text,padding,card_body_size):

  offset = 0
  #body_font = ImageFont.truetype("/Library/Fonts/Arial.ttf", size=BODY_SIZE)
  #body_font = ImageFont.truetype('/Users/mullige1/Library/Fonts/Symbola.ttf', size=RESULT_TEXT_SIZE)

  total_width = card_body_size[0]
  total_height = card_body_size[1]

  #for line in textwrap.wrap(body_text, width=CHARS_PER_LINE):
  #  total_height += body_font.getsize(line)[1]

  text = replace_unicode_icons(body_text)

  icon = Image.new("RGBA", (total_width, total_height))
  draw = ImageDraw.Draw(icon)

  autoSizeTextMultiline(draw, text, (0,0), (total_width, total_height),
                        fonttype = '/Users/mullige1/Library/Fonts/Symbola.ttf', default_size = RESULT_TEXT_SIZE, x_center=True, y_center = True)

  #for line in textwrap.wrap(text, width=CHARS_PER_LINE):
  #  w, h Im= draw.textsize(line, font=body_font)
  #  draw.text((padding+int(total_width/2-w/2), offset), line, font=body_font, fill=(0, 0, 0, 255))
  #  offset += body_font.getsize(line)[1]

  return icon


def make_monster_ability_icon(ability_line, total_width, total_height):
  #font_file = "/System/Library/Fonts/Menlo.ttc"
  font_file = '/Users/mullige1/Library/Fonts/Symbola.ttf'
  result_font = ImageFont.truetype(font_file, size=RESULT_TEXT_SIZE)
  #result_font = ImageFont.truetype("",  size=RESULT_TEXT_SIZE)
  icon = Image.new("RGBA", (total_width, total_height))
  draw = ImageDraw.Draw(icon)

  ability_line = replace_unicode_icons(ability_line)
  result_width, result_height = draw.textsize(ability_line, font=result_font)
  y_offset = int(total_height/2-result_height/2)
  draw.rectangle((0, 0, total_width, total_height),fill="rgb(255, 255, 255)", outline="rgb(0,0,0)", width=2)

  autoSizeTextMultiline(draw, ability_line, (10,0), (total_width,total_height),
                        fonttype =font_file, default_size = HEADER_FONT_SIZE, y_center = True)
  #draw.text((20, y_offset), ability_line, font=result_font, fill=(0,0,0,255))
  return icon

def make_dice_line_icon(dice_list):


  padding = 50
  total_width = 2*padding
  total_height = 0
  DICE_GAP = 40
  W2 = 10

  dice_font = ImageFont.truetype("/Library/Fonts/Arial.ttf", size=DICE_TEXT_SIZE)
  result_font = ImageFont.truetype('/Users/mullige1/Library/Fonts/Symbola.ttf', encoding="Apple Roman", size=RESULT_TEXT_SIZE)
  has_results = False
  for d in dice_list:

    d['result'] = d['result'][1:-1]

    if d['result'] is not None and len(d['result'])>0:
      has_results = True
    #die_width = d['width']
    #if result_text is not None and len(result_text)>0:
    #  die_width+=40
    total_width += d['width']+DICE_GAP
    total_height = DICE_SIZE

  if has_results:
    total_height += 80
  icon = Image.new("RGBA", (total_width, total_height))
  draw = ImageDraw.Draw(icon)


  x_offset = padding
  y_offset = 0
  for d in dice_list:
    #die_width = 40
    result_text = replace_unicode_icons(d['result'])
    print(result_text)
    die_width = d['width']
    if d['type']=='die':
      #die_width += DICE_SIZE
      draw.rectangle((x_offset, y_offset, x_offset + d['width'], y_offset+DICE_SIZE),outline="rgb(0,0,0)", width=4)

    if d['type']=='optional':
      #die_width += DICE_SIZE
      draw.rectangle((x_offset, y_offset, x_offset + d['width'], y_offset+DICE_SIZE),fill="rgb(172, 172, 172)", outline="rgb(0,0,0)", width=4)

    if d['type']=='text':
      die_width = d['width']+DICE_GAP

    autoSizeTextMultiline(draw, d['text'], (x_offset,y_offset), (die_width, DICE_SIZE), fonttype = "/Library/Fonts/Arial.ttf",
                              default_size = DICE_TEXT_SIZE, y_center = True, x_center = True)

    if result_text is not None and len(result_text)>0:
      print('hasresult')
      autoSizeTextMultiline(draw, result_text, (x_offset-20,y_offset+DICE_SIZE), (DICE_SIZE+40, 80), fonttype = '/Users/mullige1/Library/Fonts/Symbola.ttf',
                              default_size = DICE_TEXT_SIZE, y_center = True, x_center = True)
      #die_width += 40


    #x_text_offset = int(d['width']/2)-14*len(d['text'])

    #x_result_offset = int(d['width']/2)-7*len(d['result'])
    #y_text_offset = int(DICE_SIZE/2-DICE_TEXT_SIZE/2)


    #result_width, result_height = draw.textsize(result_text, font=result_font)
    #x_result_offset = int(DICE_SIZE/2-result_width/2)
    #print(DICE_SIZE,d['width'],result_width,x_result_offset)

    #print(text.encode("utf-8"))
    #draw.text((x_offset+x_text_offset, y_offset+y_text_offset), d['text'], font=dice_font, fill=(0,0,0,255))

    #draw.text((x_offset+x_result_offset, y_offset+DICE_SIZE+20), result_text, font=result_font, fill=(0,0,0,255))
    #die_width = max(d['width'],W2*len(result_text))

    x_offset += die_width+DICE_GAP
    #y_offset += 50# d['height']+20

  return icon


def replace_unicode_icons(text):
    text = text.replace("$D", '\u26CF') # Damage '\u2694''\u2694' "1F528" '\U0001F528' '\u26CF'
    text = text.replace("$R", '\u2196') # Range '\u2196' "\U0001F3F9"
    text = text.replace("$T", '\u25CE') # Targets
    text = text.replace("$B", '\u2622') # Burn
    text = text.replace("$H", '\u2665') # Heal
    text = text.replace("$M", '\U0001F6B6') # '#'\u2658') # Move "\U0001F97E"
    text = text.replace("$1", '\u2680') # Dice face 1
    text = text.replace("$2", '\u2681') # Dice face 2
    text = text.replace("$3", '\u2682') # Dice face 3
    text = text.replace("$4", '\u2683') # Dice face 4
    text = text.replace("$5", '\u2684') # Dice face 5
    text = text.replace("$6", '\u2685') # Dice face 6
    text = text.replace("$W", '\u26A0') # Threat
    text = text.replace("$A", '\U0001F6E1') # Armor
    text = text.replace("$F", '\u270A') # Retaliate
    text = text.replace("$K", '\u26E8') # Block
    text = text.replace("$C", '\u21BA') # Cooldown
    text = text.replace("$J", '\u21B7') # Jump


    return text


def autoSizeText(draw, txt, offset, size, fonttype = "/Library/Fonts/Arial.ttf", default_size = HEADER_FONT_SIZE, color = (0, 0 , 0, 255) ):
  fontsize = default_size
  font = ImageFont.truetype(fonttype, fontsize)
  while font.getsize(txt)[1] > size[1] or font.getsize(txt)[0] > size[0]:
    fontsize -= 1
    font = ImageFont.truetype(fonttype, fontsize)

  draw.text(offset, txt, font=font, fill= color)


def autoSizeTextMultiline(draw, txt, offset, size, fonttype = "/Library/Fonts/Arial.ttf",
                          default_size = HEADER_FONT_SIZE, y_center = False, x_center = False, color=(0,0,0,255)):
  fontsize = default_size
  font = ImageFont.truetype(fonttype, fontsize)

  fits = False

  while not fits:
    w, h = font.getsize(txt)
    wc = w / len(txt)
    n_char = int(size[0] / wc)
    lines = textwrap.wrap(txt, width=n_char)
    total_height = len(lines)*h
    total_width = n_char * wc
    fits = total_height < size[1] and total_width < size[0]
    fontsize -= 1
    font = ImageFont.truetype(fonttype, fontsize)
    if fontsize < 1:
      break

  if y_center:
    y_offset = offset[1]+int(size[1]/2-total_height/2)
  else:
    y_offset = offset[1]

  for line in lines:
    w, h = draw.textsize(line, font=font)
    if x_center:
      x_offset = offset[0]+int(size[0]/2-w/2)
    else:
      x_offset = offset[0]
    draw.text((x_offset,y_offset), line, font=font, fill=color)
    y_offset += font.getsize(line)[1]
