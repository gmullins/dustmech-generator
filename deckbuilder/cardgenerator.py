import os
import PIL
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
from PIL import ImageColor
import textwrap
import re


CHARS_PER_LINE = 26
FONT_PATH = "/Library/Fonts/Arial.ttf"
BODY_SIZE = 50
FOOTER_SIZE = 18
HEADER_FONT_SIZE = 60
HEADER_SIZE = 120
KEYWORD_SIZE = 32
#Name,Type,Keywords,Power,Move,Body,Discard

class Card(object):
  def __init__(self,config):
    self.name = config['Name']
    self.type = config['Type']
    self.keywords_text = self.get_default(config,"Keywords","")
    self.power_cost = self.get_default(config,"Power","0")
    self.move_cost = self.get_default(config,"Move","0")
    self.body_text = self.get_default(config,"Body","Text goes here")
    self.bottom_text = self.get_default(config,"Discard",None)
    self.front_img = None
    self.back_img = None

  def get_default(self,config,field,default):
    new_line = config.get(field,default)
    if(new_line == '' or new_line == '-' ):
          new_line = None
    return new_line


class SheetGenerator(object):
  def __init__(self,config):
    self.num_rows = 2
    self.num_columns = 4
    self.file_type = "PDF"
    self.card_dimensions = config["card_dimensions_px"]
    self.inter_x_pad = config.get('inter_x_pad',0)
    self.inter_y_pad = config.get('inter_y_pad',0)
    self.left_x_pad = config.get('inter_x_buff',130)
    self.right_x_pad = config.get('inter_x_buff',265)
    self.bottom_y_pad = config.get('inter_x_buff',150)
    self.top_y_pad = config.get('inter_x_buff',140)
    self.double_sided = config.get('double_sided',False)
    self.cards = []
    self.index = 0
    self.color = config['color']
    self.save_folder = config["save_folder"]
    self.sheet_list = []
    self.sheet_name = "CardSheet"

  @property
  def background_color(self):
    if self.color == "white":
      return (255, 255, 255, 255)
    else:
      return (0, 0, 0, 255)

  def add_card(self,card):
    self.cards.append(card)

  def makeSheets(self):
    X_SIZE = self.card_dimensions[0]*self.num_columns+self.left_x_pad+self.right_x_pad+3*self.inter_x_pad
    Y_SIZE = self.card_dimensions[1]*self.num_rows+self.top_y_pad+self.bottom_y_pad+self.inter_y_pad
    sheet = Image.new("RGB", (X_SIZE, Y_SIZE), self.background_color)
    draw = ImageDraw.Draw(sheet)

    if(self.double_sided):
      backsheet = Image.new("RGB", (X_SIZE, Y_SIZE), self.background_color)
      draw_back = ImageDraw.Draw(backsheet)

    x_offset = self.left_x_pad
    y_offset = self.top_y_pad
    for card in self.cards:
      card_img = card.front_img
      sheet.paste(card_img, (x_offset, y_offset), card_img)
      draw.line((x_offset,0,x_offset,Y_SIZE),fill="rgb(10,18,25)",width=5)
      draw.line((x_offset+card_img.size[0],0,x_offset+card_img.size[0],Y_SIZE),fill="rgb(10,18,25)",width=5)
      draw.line((0,y_offset,X_SIZE,y_offset),fill="rgb(10,18,25)",width=5)
      draw.line((0,y_offset+card_img.size[1],X_SIZE,y_offset+card_img.size[1]),fill="rgb(10,18,25)",width=5)
      if self.double_sided:
        card_img = card.back_img
        backsheet.paste(card_img, (x_offset, y_offset), card_img)
        draw_back.line((x_offset,0,x_offset,Y_SIZE),fill="rgb(10,18,25)",width=5)
        draw_back.line((x_offset+card_img.size[0],0,x_offset+card_img.size[0],Y_SIZE),fill="rgb(10,18,25)",width=5)
        draw_back.line((0,y_offset,X_SIZE,y_offset),fill="rgb(10,18,25)",width=5)
        draw_back.line((0,y_offset+card_img.size[1],X_SIZE,y_offset+card_img.size[1]),fill="rgb(10,18,25)",width=5)

      x_offset +=card_img.size[0]+self.inter_x_pad
      if(x_offset+self.right_x_pad >= X_SIZE):
        x_offset=self.left_x_pad
        y_offset +=card_img.size[1]+self.inter_y_pad
      if(y_offset+self.bottom_y_pad >= Y_SIZE):
        self.sheet_list.append(sheet)
        if self.double_sided:
          self.sheet_list.append(backsheet)
        y_offset=self.top_y_pad
        x_offset=self.left_x_pad
        sheet = Image.new("RGB", (X_SIZE, Y_SIZE), self.background_color)
        draw = ImageDraw.Draw(sheet)
        if(self.double_sided):
          backsheet = Image.new("RGB", (X_SIZE, Y_SIZE), self.background_color)
          draw_back = ImageDraw.Draw(backsheet)


    self.sheet_list.append(sheet)
    if self.double_sided:
      self.sheet_list.append(backsheet)


  def writeSheets(self):
    filename = u"{}.pdf".format(self.sheet_name)
    self.index += 1
    self.makeSheets()

    if self.file_type == 'PDF':
      self.sheet_list[0].save(os.path.join(self.save_folder, filename),'PDF', dpi=(300, 300),save_all=True, append_images=self.sheet_list[1:])

    if self.file_type == 'PNG':
      for i,sheet in enumerate(self.sheet_list):
        filename = u"{}.{}.png".format(self.sheet_name,i)
        sheet.save(os.path.join(self.save_folder, filename))

class CardGenerator(object):
  def __init__(self, config):
    self.save_folder = config["save_folder"]
    self.color = config["color"]
    self.card_dimensions = config["card_dimensions_px"]

    self.card_colors = {'System':"rgb(155,155,255)",'Energy':"rgb(235,235,0)",
                        "Opposition":"rgb(235,40,100)",'Module':"rgb(50,235,130)",
                        "Item":"rgb(150,150,150)"}
    self.icons = {'Power': self.make_power_icon(), 'Move':self.make_move_icon(),
                  'Double_Power':self.make_double_power_icon(), 'Double_Move':self.make_double_move_icon(),
                  'Threat': self.make_threat_icon()}
    self.card_body_size = {'default':BODY_SIZE, 'System':BODY_SIZE,'Energy':270}

    self.footer_text = "DustMech v0.3.13"
    self.index = 0
    self.padding = config["card_padding_px"]
    self.power_icon =  self.make_power_icon()
    self.move_icon = self.make_move_icon()

    super(CardGenerator, self).__init__()


  def make_power_icon(self):
    #return Image.open('input/bluediamond.png','r')
    RECT_SIZE = 256
    icon = Image.new("RGBA", (RECT_SIZE, RECT_SIZE))
    draw = ImageDraw.Draw(icon)
    r=0.15
    draw.rectangle((RECT_SIZE*r, RECT_SIZE*r, RECT_SIZE*(1-r),RECT_SIZE*(1-r)), fill="rgb(100,180,255)",outline="rgb(25,25,55)", width=20)
    icon = icon.rotate(45)
    return icon

  def make_double_power_icon(self):
    #return Image.open('input/bluediamond.png','r')
    RECT_SIZE = 1024
    icon = Image.new("RGBA", (RECT_SIZE, RECT_SIZE))
    draw = ImageDraw.Draw(icon)
    a=0.0
    b=1/3
    c=0.26
    d=0.05
    x0 = a*RECT_SIZE
    x1 = RECT_SIZE*b
    x2 = RECT_SIZE/2
    x3 = RECT_SIZE*(1-b)
    x4 = RECT_SIZE*(1-a)
    y0 = RECT_SIZE/2
    y1 = RECT_SIZE*(1-d)
    y2 = RECT_SIZE*(1-c)
    y3 = RECT_SIZE*(c)
    y4 = RECT_SIZE*(d)

    xy = [(x0,y0),(x1,y1),(x2,y2),(x3,y1),(x4,y0),(x3,y4),(x2,y3),(x1,y4),(x0,y0)]
    draw.polygon(xy, fill="rgb(100,180,255)",outline="rgb(25,25,55)")
    draw.line(xy, fill="rgb(10,18,25)",width=50)

    return icon


  def make_threat_icon(self):
    #return Image.open('input/bluediamond.png','r')
    RECT_SIZE = 1024
    icon = Image.new("RGBA", (RECT_SIZE, RECT_SIZE))
    draw = ImageDraw.Draw(icon)
    a=0.0
    b=0.0
    x0 = a*RECT_SIZE
    x1 = RECT_SIZE*(1-a)
    x2 = RECT_SIZE/2

    y0 = b*RECT_SIZE
    y1 = RECT_SIZE*(1-b)

    xy = [(x0,y0),(x1,y0),(x2,y1),(x0,y0)]
    draw.polygon(xy, fill="rgb(255,100,1)",outline="rgb(25,25,55)")
    draw.line(xy, fill="rgb(10,18,25)",width=50)

    return icon


  def make_move_icon(self):
    RECT_SIZE = 1024
    icon = Image.new("RGBA", (RECT_SIZE, RECT_SIZE))
    draw = ImageDraw.Draw(icon)
    draw.ellipse((0, 0, RECT_SIZE,RECT_SIZE), fill="rgb(100,255,180)",outline="rgb(5,5,5)", width=45)
    return icon

  def make_double_move_icon(self):
    return self.make_move_icon()

  @property
  def background_color(self):
    if self.color == "white":
      return (255, 255, 255, 255)
    else:
      return (0, 0, 0, 255)

  @property
  def foreground_color(self):
    if self.color == "white":
      return (0, 0, 0, 255)
    else:
      return (255, 255, 255, 255)


  def placeIcon(self, card_img, draw, offset, cost_text, size, icon_name, alignment="left"):

    icon_font =  ImageFont.truetype("/Library/Fonts/Arial.ttf", int(size/2))
    cost_width  =  icon_font.getsize(cost_text)[0]
    cost_height = icon_font.getsize(cost_text)[1]
    icon = self.icons[icon_name]
    size_x = size
    size_y = size
    if(cost_width*1.2>size):
        icon = self.icons['Double_'+icon_name]
        size_x = int(cost_width*1.5)


    powercell_img = icon.resize((size_x,size_y),Image.LANCZOS)
    icon_font =  ImageFont.truetype("/Library/Fonts/Arial.ttf", int(size/2))

    if alignment=="left":
      cost_img_offset_x = offset[0]
    else:
      cost_img_offset_x = offset[0]-size_x

    cost_img_offset_y = offset[1]

    cost_text_offset_x = cost_img_offset_x+powercell_img.size[0]/2-cost_width/2
    cost_text_offset_y = cost_img_offset_y+powercell_img.size[1]/2-cost_height/1.75

    card_img.paste(powercell_img, (cost_img_offset_x, cost_img_offset_y), powercell_img)
    draw.text((cost_text_offset_x, cost_text_offset_y), cost_text, font=icon_font, fill=self.foreground_color)


    if alignment=="left":
      return int(cost_text_offset_x)+size_x
    else:
      return int(cost_text_offset_x)

  def placePowerIcon(self, card_img, draw, offset, cost_text, size, alignment="left"):
    offset_x = self.placeIcon(card_img, draw, offset, cost_text, size, 'Power', alignment)
    return offset_x

  def placeMoveIcon(self, card_img, draw, offset, cost_text, size, alignment="left"):
    offset_x = self.placeIcon(card_img, draw, offset, cost_text, size, 'Move', alignment)
    return offset_x

  def placeThreatIcon(self, card_img, draw, offset, cost_text, size, alignment="left"):
    offset_x = self.placeIcon(card_img, draw, offset, cost_text, size, 'Threat', alignment)
    return offset_x

  def autoSizeText(self, draw, txt, offset, size, fonttype = "/Library/Fonts/Arial.ttf", default_size = HEADER_FONT_SIZE):
    fontsize = default_size
    font = ImageFont.truetype(fonttype, fontsize)
    while font.getsize(txt)[1] > size[1] or font.getsize(txt)[0] > size[0]:
      fontsize -= 1
      font = ImageFont.truetype(fonttype, fontsize)

    draw.text(offset, txt, font=font, fill=self.foreground_color)


  def makeHeader(self, card_data, card_img, draw, yoffset = 0):
    header_font = ImageFont.truetype("/Library/Fonts/Arial.ttf", HEADER_FONT_SIZE)
    offset = yoffset+self.padding
    header_bottom = offset + header_font.getsize(card_data.name)[1]
    draw.rectangle((0, 0, card_img.size[0], HEADER_SIZE), fill=self.card_colors[card_data.type])

    offset = offset + HEADER_SIZE
    cost_icon_offset_x = card_img.size[0]


    self.autoSizeText(draw, card_data.name, (self.padding, self.padding), (cost_icon_offset_x-self.padding,HEADER_SIZE), default_size=HEADER_FONT_SIZE)

    if(card_data.move_cost != "0"  and card_data.move_cost is not None):
      cost_icon_offset_x = self.placeMoveIcon(card_img, draw, (cost_icon_offset_x, 0), card_data.move_cost, HEADER_SIZE, alignment="right")
    if(card_data.power_cost != "0" and card_data.power_cost is not None ):
      cost_icon_offset_x = self.placePowerIcon(card_img, draw, (cost_icon_offset_x, 0), card_data.power_cost, HEADER_SIZE, alignment="right")




    draw.line((0, offset, card_img.size[0], offset), fill=128)
    return offset

  def makeKeywords(self, card_data, card, draw, yoffset = 0):
    keyword_font = ImageFont.truetype("/Library/Fonts/Arial.ttf", KEYWORD_SIZE)
    card_type_font = ImageFont.truetype("/Library/Fonts/Arial.ttf", int(KEYWORD_SIZE*1.25))

    card_type_bottom = card_type_font.getsize(card_data.type)[1]

    draw.rectangle((0, yoffset, card.size[0],yoffset+card_type_bottom),fill="rgb(200,200,200)")
    draw.text((self.padding, yoffset), card_data.type, font=card_type_font, fill=self.foreground_color)
    if card_data.keywords_text is not None:
      keyword_width = keyword_font.getsize(card_data.keywords_text)[0]
      keyword_bottom = keyword_font.getsize(card_data.keywords_text)[1]
      draw.text((card.size[0]-keyword_width-self.padding/3, yoffset+card_type_bottom-keyword_bottom), card_data.keywords_text, font=keyword_font, fill=self.foreground_color)

    yoffset = yoffset+card_type_bottom
    return yoffset


  def makeBottomText(self, card_data, card_img, draw):
    bottom_font = ImageFont.truetype("/Library/Fonts/Arial.ttf", HEADER_FONT_SIZE)

    bottom_text_width = bottom_font.getsize(card_data.bottom_text)[0]
    bottom_text_height = bottom_font.getsize(card_data.bottom_text)[1]

    draw.rectangle((0, card_img.size[1] - bottom_text_height - self.padding / 2, card_img.size[0], card_img.size[1]), fill="rgb(200,100,100)")
    offset = (self.padding / 3, card_img.size[1] - bottom_text_height - self.padding / 3)
    self.printWithIcons(card_img,draw,offset, card_data.bottom_text,bottom_font)

    #draw.text((self.padding / 3, card_img.size[1] - bottom_text_height - self.padding / 3), card_data.bottom_text, font=bottom_font, fill=self.foreground_color)


  def printWithIcons(self,card_img,draw,offset,line,font,alignment="left"):
        offset_x = int(offset[0])
        offset_y = int(offset[1])
        size_y = font.getsize(line)[1]
        line = line.replace("\"","")
        elements = re.split('\<.\:.\>',line)
        m = re.findall('\<.\:.\>', line)
        text_width = font.getsize(',,,'.join(elements))[0]
        if(alignment=="center"):
          offset_x = int(offset[0]-text_width/2)
        if(alignment=="right"):
          offset_x = int(offset[0]-text_width)
        i=0
        for e in elements:
          draw.text((offset_x,offset_y), e, font=font, fill=self.foreground_color)
          offset_x += font.getsize(e)[0]
          if i < len(m):
            P = re.split('\<|\:|\>',m[i])
            i += 1
            if P[2] == 'P':
              offset_x = self.placePowerIcon(card_img, draw, (offset_x, offset_y), P[1], size_y)
            if P[2] == 'M':
              offset_x = self.placeMoveIcon(card_img, draw, (offset_x, offset_y), P[1], size_y)
            if P[2] == 'T':
              offset_x = self.placeThreatIcon(card_img, draw, (offset_x, offset_y), P[1], size_y)




  def makeCard(self,card_data):
    card_img = Image.new("RGBA", (self.card_dimensions[0], self.card_dimensions[1]), self.background_color)
    draw = ImageDraw.Draw(card_img)

    offset = self.makeHeader(card_data,card_img,draw)
    offset = self.makeKeywords(card_data,card_img,draw,offset)

    offset += self.padding
    # draw words
    body_text = card_data.body_text
    large_text = re.search('\@.*\@',body_text)
    if large_text:
      body_font = ImageFont.truetype(FONT_PATH, 250)
      lines = textwrap.wrap(body_text, width=10)
      icon_text_list = large_text[0].split('@')[1:-1]
      offset += (300)-(100)*len(icon_text_list)
      for icon_text in icon_text_list:
        self.printWithIcons(card_img,draw,(card_img.size[0]/2,offset),icon_text.replace('@',''),body_font,alignment='center')
        offset += 200
      body_text = body_text.replace(re.search('\@.*\@',body_text)[0],'')


    offset = offset + 30
    body_font = ImageFont.truetype(FONT_PATH, self.card_body_size['default'])

    for line in textwrap.wrap(body_text, width=CHARS_PER_LINE):
      self.printWithIcons(card_img,draw,(self.padding,offset),line,body_font)
      body_font = ImageFont.truetype(FONT_PATH, self.card_body_size['default'])
        #elements = re.split('\<.\:.\>',line)
        #draw.text((self.padding, offset), line, font=body_font, fill=self.foreground_color)
      offset += body_font.getsize(line)[1]

    if card_data.bottom_text is not None:
      self.makeBottomText(card_data,card_img,draw)

    # draw footer
    footer_font = ImageFont.truetype("/Library/Fonts/Arial.ttf", FOOTER_SIZE)
    footer_text_left_offset = card_img.size[0] - self.padding / 3 - footer_font.getsize(self.footer_text)[0]
    footer_text_top_offset = card_img.size[1] - self.padding / 3 - footer_font.getsize(self.footer_text)[1]
    draw.text((footer_text_left_offset, footer_text_top_offset), self.footer_text, font=footer_font, fill=self.foreground_color)
    
    return card_img

  def writeCard(self, card_data):
    filename = u"{}.{}.0.png".format(card_data.type,self.index)
    self.index += 1
    card = self.makeCard(card_data)
    card.save(os.path.join(self.save_folder, filename))
    if self.index==0:
      card_back = self.makeBack()
      filename = u"{}.1.png".format(self.index)
      card_back.save(os.path.join(self.save_folder, filename))

    return card

  def makeBack(self):
    card_img = Image.new("RGBA", (self.card_dimensions[0], self.card_dimensions[1]), self.background_color)
    draw = ImageDraw.Draw(card_img)

    # draw words
    body_font = ImageFont.truetype(FONT_PATH, BODY_SIZE * 2)

    placeholder_logo = Image.open('input/wrench_cog_icon.png','r')

    offset_x = int(card_img.size[0]/2-placeholder_logo.size[0]/2)
    offset_y = int(card_img.size[1]/2-placeholder_logo.size[1]/2)
    card_img.paste(placeholder_logo, (offset_x, offset_y), placeholder_logo)

    return card_img

  def writeBack(self):
    card = self.makeBack()
    card.save(os.path.join(self.save_folder, "back.{}.png".format(self.color)))
